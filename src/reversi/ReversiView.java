package reversi;

import java.awt.GridLayout;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ReversiView extends JFrame{

	private static final int side = 600;
	private static final int rows = 8,cols=rows;
	private static String playerOneSign="X",playerTwoSign="O";
	private static JButton[][] buttons;
	private static long timeOut;
	
	private ArrayList<Point> legal= new ArrayList<Point>();

	public static void main(String[] args) {
		timeOut = Long.MAX_VALUE;
		boolean test = false;
		boolean AIvsAI = true;
		boolean chatty = true;
		if(args.length==2){
			
			test=args[1].equals("-t");
		}
		
		
		ReversiView view = new ReversiView();
		
		if(!test){
			boolean gotAnswer = false;
			while (!gotAnswer) {
				gotAnswer = true;
				String s = (String) JOptionPane
						.showInputDialog(
								view,
								"Timeout value (leave blank for default @ 292 billion years):",
								"Timeout", JOptionPane.PLAIN_MESSAGE, null, null,
								null);
				if (s == null || s.isEmpty())
					break;
				try {
					timeOut = Long.valueOf(s);
				} catch (NumberFormatException e) {
					System.out.println("fail");
					gotAnswer = false;
				}
			}
			String[] options = { "Player vs AI", "AI vs AI" };
			int choice = JOptionPane.showOptionDialog(view, "Choose players:",
					"Player configuration", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			AIvsAI = choice == 1;
			String[] options2 = { "Verbose", "Quiet" };
			choice = JOptionPane.showOptionDialog(view,
					"How chatty should the AI be:", "AI configuration",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
					options2, options2[0]);
			chatty = choice == 0;
		}
		

		ReversiGame game = new ReversiGame();
		HAL oracle = new HAL(game, timeOut, ReversiGame.PLAYER_TWO_ID, chatty);
		JPanel panel = new JPanel();

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridLayout(rows, cols, 5, 5));

		buttons = new JButton[rows][cols];
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				buttons[row][col] = new JButton();
				if (!AIvsAI)
					buttons[row][col]
							.addActionListener(new ButtonActionListener(view,
									game, oracle, row, col));
				panel.add(buttons[row][col]);
			}
		}

		view.add(panel);
		view.setVisible(true);

		if (!AIvsAI) {
			System.out.println("You are playing as " + playerOneSign);
			view.updateBoard(game.getBoard(), game.getScore()[0],
					game.getScore()[1],
					game.getAllLegalMoves(ReversiGame.PLAYER_ONE_ID));
		} else {
			HAL hero = new HAL(game, timeOut, ReversiGame.PLAYER_ONE_ID, chatty);
			while (!game.isGameOver()) {

				Point move = hero.makePlay();
				if (game.isGameOver())
					break;

				game.play(move.x, move.y, ReversiGame.PLAYER_ONE_ID);
				view.updateBoard(game.getBoard(), game.getScore()[0],
						game.getScore()[1], new ArrayList<Point>());

				if (game.isGameOver())
					break;

				move = oracle.makePlay();
				if (game.isGameOver())
					break;
				game.play(move.x, move.y, ReversiGame.PLAYER_TWO_ID);
				view.updateBoard(game.getBoard(), game.getScore()[0],
						game.getScore()[1], new ArrayList<Point>());
			}
			view.gameOver(game.getScore(ReversiGame.PLAYER_ONE_ID),
					game.getScore(ReversiGame.PLAYER_TWO_ID));
		}

	}	
	
	public ReversiView(){
		setTitle("Beat me if you can.");
	    setSize(side, side);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);	    	    
	}

	public void updateBoard(int[][] board, int scoreOne, int scoreTwo,ArrayList<Point> legal) {
        for (int row = 0; row < board.length; row++) {
        	for(int col = 0; col< board[row].length;col++){
        		if(board[row][col]==ReversiGame.PLAYER_ONE_ID){
        			buttons[row][col].setText(playerOneSign);
        		}else if(board[row][col]==ReversiGame.PLAYER_TWO_ID){
        			buttons[row][col].setText(playerTwoSign);
        		}
        			
        	}
        }
        updateLegalMoves(legal);
	}
	
	public void gameOver(int scoreOne,int scoreTwo){
		System.out.println("Game over");
		System.out.println(playerOneSign+" score :"+scoreOne+"\t"+playerTwoSign+" score :"+scoreTwo);
		System.out.println("Mean searchtime:"+ HAL.getMeanSearchTime());
	}

	public void updateLegalMoves(ArrayList<Point> l) {
		//clear previous
		
		for(Point p : legal){
			if(buttons[p.x][p.y].getText().equals("HERE"))
				buttons[p.x][p.y].setText("");
		}
		legal=l;
		for(Point p : legal){
			if(buttons[p.x][p.y].getText().equals("")){
				buttons[p.x][p.y].setText("HERE");
			}				
			
		}
	}
}
