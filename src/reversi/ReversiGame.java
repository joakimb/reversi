package reversi;

import java.awt.Point;
import java.util.ArrayList;

public class ReversiGame {
	
	private int playerOneScore =0,playerTwoScore=0;
	private int[][] gameState;
	private boolean gameOver = false;
	
	public static final int PLAYER_ONE_ID=1, PLAYER_TWO_ID=2;
	private static final int rows = 8,cols=rows;	
	private final static int DX=0,DY=1;
	private final static int WEST=-1,EAST=1,NORTH=1,SOUTH=-1;
	private final static int[][] DIRECTIONS= {{0,NORTH},//North
												{WEST,NORTH},//NORTHWEST
												{WEST,0},//WEST
												{WEST,SOUTH},//SOUTHWEST
												{0,SOUTH},//SOUTH
												{EAST,SOUTH},//SOUTHEAST
												{EAST,0},//EAST
												{EAST,NORTH}};//NORTHEAST


	
	public ReversiGame(){
		gameState = new int[rows][cols];
		
		gameState[rows/2 - 1][cols/2 - 1] = PLAYER_TWO_ID;
		gameState[rows/2][cols/2] = PLAYER_TWO_ID;
		gameState[rows/2 - 1][cols/2] = PLAYER_ONE_ID;
		gameState[rows/2][cols/2 - 1] = PLAYER_ONE_ID;
		
	}
	
	public ReversiGame copy(){
		ReversiGame rg = new ReversiGame();
		for (int i = 0; i < this.gameState.length; i++) {
			System.arraycopy(this.gameState[i], 0, rg.gameState[i], 0, this.gameState[i].length);
		}
		return rg;
	}
	
	public boolean play(int x, int y,int player){
		
		if(gameState[x][y] != 0){
			return false;
		}
		ArrayList<Point> list = new ArrayList<Point>();
		for(int[] direction: DIRECTIONS){
			ArrayList<Point> dirList =getFLips(x+direction[DX],y+direction[DY],direction[DX],direction[DY],player);
			if(dirList!= null && !dirList.isEmpty())
				list.addAll(dirList);
		}
		if(list.isEmpty())			
			return false;
		
		list.add(new Point(x,y));
		flipTheFlips(list,player);
		updateScore();
		return true;
	}
	private ArrayList<Point> getAllFlipsFromMove(int x, int y,int player){
		
		ArrayList<Point> list = new ArrayList<Point>();
		for(int[] direction: DIRECTIONS){
			ArrayList<Point> dirList =getFLips(x+direction[DX],y+direction[DY],direction[DX],direction[DY],player);
			if(dirList!= null && !dirList.isEmpty())
				list.addAll(dirList);
		}		
		
		if(list.isEmpty())
			return null;
		list.add(new Point(x,y));
		return list;
	}
	
	public ArrayList<Point> getAllLegalMoves(int player) {
		ArrayList<Point> legalMovesList=new ArrayList<Point>();
		
			for(int x = 0; x<rows; x++){
				for(int y= 0; y<cols; y++){
					if(gameState[x][y]==0 && getAllFlipsFromMove(x, y, player)!= null)
						legalMovesList.add(new Point(x,y));
				}
			}
		
		gameOver=gameOver || legalMovesList.isEmpty();
		
		return legalMovesList;
		
	}

	private void updateScore() {
		playerOneScore=playerTwoScore=0;
		for(int[] rows:gameState){
			for(int brick:rows){
				if(brick==PLAYER_ONE_ID)
					playerOneScore++;
				else if(brick==PLAYER_TWO_ID)
					playerTwoScore++;
			}
		}
		gameOver= gameOver || (playerOneScore+playerTwoScore)==(rows*cols) || (playerOneScore*playerTwoScore) == 0  ;
	}
	
	
	private void flipTheFlips(ArrayList<Point> list, int player) {
		for(Point p:list)
			gameState[p.x][p.y]=player;				
	}

	private ArrayList<Point> getFLips(int x, int y, int dx, int dy, int player){
		ArrayList<Point> list = null;
		if(x >= rows || y >= cols || x<0 || y<0|| gameState[x][y] == 0){
			//X,Y is outside the board or gameState[x][y] is not taken
			return null;
		} else if (gameState[x][y] == player){
			//X,Y belongs to the player, thus end the recursion
			list= new ArrayList<Point>();
		} else {
			//X,Y belongs to adversary
			//continue until we find something that belongs to us or return null if not
			list = getFLips(x + dx, y + dy, dx, dy, player);
			if (list == null) return null;
			list.add(new Point(x, y));
		}
		return list;
	}
	
	public int[][] getBoard(){
		return gameState;
	}
	public int[] getScore(){
		return new int[]{playerOneScore,playerTwoScore};
	}

	public int getScore(int player) {		
		return player==PLAYER_ONE_ID?playerOneScore:playerTwoScore;
	}	
	public boolean isGameOver(){
		return gameOver;
	}
}
