package reversi;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ButtonActionListener implements ActionListener{
	private ReversiGame myGame;
	private ReversiView myView;
	private HAL myOracle;
	private int x,y;
	
	public ButtonActionListener(ReversiView aView,ReversiGame aGame,HAL aOracle, int x, int y){
		super();
		myGame=aGame;
		myView=aView;
		myOracle=aOracle;
		
		this.x=x;
		this.y=y;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(!myGame.play(x, y, 1)){
			System.out.println("Illegal move, try again");
			return;
		}
		myView.updateBoard(myGame.getBoard(),myGame.getScore()[0],myGame.getScore()[1],new ArrayList<Point>());
		System.out.println("score p1: "+myGame.getScore(ReversiGame.PLAYER_ONE_ID)+" p2: "+myGame.getScore(ReversiGame.PLAYER_TWO_ID));
		
		Point oMove = myOracle.makePlay();
		if(!myGame.isGameOver()){
			System.out.println(myGame.isGameOver());
			myGame.play(oMove.x, oMove.y, ReversiGame.PLAYER_TWO_ID);
			myView.updateBoard(myGame.getBoard(),myGame.getScore(ReversiGame.PLAYER_ONE_ID),myGame.getScore(ReversiGame.PLAYER_TWO_ID),myGame.getAllLegalMoves(ReversiGame.PLAYER_ONE_ID));
		}
		if(myGame.isGameOver())
			myView.gameOver(myGame.getScore(ReversiGame.PLAYER_ONE_ID),myGame.getScore(ReversiGame.PLAYER_TWO_ID));
		
	}
}
