package reversi;

import java.awt.Point;
import java.util.ArrayList;

public class HAL {
	private long timeOut;
	private long cockyTime;
	private long nervousTime;
	private long startTime;
	private ReversiGame game;
	private int player;
	private boolean chatty;
	public static long searchTime;
	public static long plays;
	
	public static long getMeanSearchTime(){
		return searchTime/plays;
	}
	
	/*
	 * Constructor for player two AI
	 * param ReversiGame game 	- the current state of the game used to search one
	 * param long timeOut 		- time in ms used to timeout search (instead of a certain depth) 
	 */
	public HAL(ReversiGame game, long timeOut,int player,boolean chatty){
		this.timeOut=timeOut;
		cockyTime=timeOut/5;
		nervousTime=timeOut-cockyTime;	
		this.game = game;
		this.player=player;
		this.chatty=chatty;
	}
	
	/*
	 * Ask player two for its move. Invoked after player one have put its piece. 
	 */
	public Point makePlay() {
		if(chatty)
			System.out.println("thinking ...");
		startTime = System.currentTimeMillis();
		
		//Root of the tree
		HalNode root = new HalNode(game, null,player);
		//alpha and beta start values for pruning the three
		int alpha=Integer.MIN_VALUE,  beta= Integer.MAX_VALUE;		
		root.search(true,alpha,beta);
		
		long T2 = System.currentTimeMillis();
		if(chatty)
			talk(T2-startTime);
		
		searchTime+=(T2-startTime);
		plays++;
		return root.bestMove;
	}

	/*
	 * Used by player two to give feedback about how stressed its choice was.
	 * param long time - time for the search in ms. 
	 */
	private void talk(long time){
		if(time<=cockyTime){
			System.out.println("yeah, thought of this move in no time dude");
		}else if(time <=nervousTime){
			System.out.println("Easy choice");
		}else{
			System.out.println("ugh..");
		}
	}
	
	/*private class representing each node in the tree*/
	private class HalNode{
		private int player;
		private ReversiGame currState; 	//gamestate for each node - memory is no problem :))
		public Point myMove; 			//move that created this state
		public Point bestMove;			//best move from all children (thus the roots bestMove will be the one to finally place)
		
		
		/*
		 * Constructor for each node.
		 * param ReversiGame nodeState 	- the state for this node.
		 * param Point move				- the move on the parent state that created this child state. 
		 */
		public HalNode(ReversiGame nodeState, Point move,int player){
			this.currState = nodeState;
			myMove = move;
			this.player=player;
		}
		
		
		/*
		 * Search method. This method is invoked recursively to traverse the
		 * tree depth-first. Entire branches are pruned with Alpha-beta method,
		 * thus player two skips branches where player one will come out on
		 * top.(st�mmer de?) (detta �r expend(depth) bara nytt namn. This
		 * method terminate long searches not with a certain depth but with a
		 * time requirement. param boolean max -indicates wheter the search is
		 * made as player one or two. choosing min or max values of each branch.
		 * (thus combining both min and max functions in one) param int alpha -
		 * max score for this branch param int beta - min score for this branch
		 * ( st�mmer?)
		 */
		public int search(boolean max, int alpha, int beta) {

			// get all all children from all legal moves that can be made.
			ArrayList<Point>  children = currState.getAllLegalMoves(player);

			// If no legal moves can be done or we have reached our time limit.
			if (children.isEmpty()
					|| (System.currentTimeMillis() - startTime) > timeOut) {
				int adversaryPlayer=(player==ReversiGame.PLAYER_TWO_ID)?ReversiGame.PLAYER_ONE_ID:ReversiGame.PLAYER_TWO_ID;				
				int score = currState.getScore(player)-currState.getScore(adversaryPlayer);
				bestMove = myMove;
				return score;
			}

			// choose min/max children or terminate if beta<=alpha
			int v =max? Integer.MIN_VALUE:Integer.MAX_VALUE;

			for (int child = 0; child < children.size(); child++) {
				HalNode nextNode = generateNextChild(currState, children.get(child), player);
				int score = nextNode.search(!max, alpha, beta);
				
				if(max){
					if (v < score) {
						v = score;
						bestMove = nextNode.myMove;
					}
					if(v>=beta)
						return v;
					
					alpha = (alpha < v) ? v : alpha;
					
				}else{
					if (v > score) {
						v = score;
						bestMove = nextNode.myMove;
					}
					if(v <= alpha)
						return v;
					
					beta = (beta > v) ? v:beta;
					
				}

			}
			return v;
		}
		

		private HalNode generateNextChild(ReversiGame curr, Point move,int player){			
			ReversiGame ng = curr.copy();
			ng.play(move.x, move.y, player);
			return new HalNode(ng,move,player);			
		}
	}
}
